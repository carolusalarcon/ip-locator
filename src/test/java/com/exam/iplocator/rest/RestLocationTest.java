package com.exam.iplocator.rest;

import com.exam.iplocator.model.Location;
import com.exam.iplocator.service.LocationService;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class RestLocationTest extends TestCase {

    @TestConfiguration
    static class RestLocationTestContextConfiguration {

        @Bean
        public RestLocation restLocation() {
            return new RestLocation();
        }
    }

    @Autowired
    private RestLocation restLocation;

    @MockBean
    private LocationService locationService;

    @Test
    public void testValidLocation() {
        Location.Currency currency = new Location.Currency("",null);
        Location.Country country = new Location.Country("","");
        Location location = new Location(country, currency);
        when(locationService.getLocation(anyString())).thenReturn(location);
        ResponseEntity<Location> response = restLocation.getLocation("186.97.174.42");
        assertSame(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testEmptyLocation() {
        when(locationService.getLocation(anyString())).thenReturn(null);
        ResponseEntity<Location> response = restLocation.getLocation("186.97.174.42");
        assertSame(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }

    @Test
    public void testNoValidLocation() {
        ResponseEntity<Location> response = restLocation.getLocation("186.97.174");
        assertSame(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }
}