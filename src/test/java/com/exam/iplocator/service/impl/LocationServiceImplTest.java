package com.exam.iplocator.service.impl;

import com.exam.iplocator.model.Location;
import com.exam.iplocator.retrofit.model.CountryResponse;
import com.exam.iplocator.retrofit.model.CurrencyResponse;
import com.exam.iplocator.service.BlackListService;
import com.exam.iplocator.service.CountryService;
import com.exam.iplocator.service.CurrencyService;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class LocationServiceImplTest extends TestCase {

    @TestConfiguration
    static class LocationServiceImplTestContextConfiguration {

        @Bean
        public LocationServiceImpl locationService() {
            return new LocationServiceImpl();
        }
    }


    @Autowired
    LocationServiceImpl locationService;

    @MockBean
    private CountryService countryService;

    @MockBean
    private CurrencyService currencyService;

    @MockBean
    private BlackListService blackListService;

    @Test
    public void testInBlackList() throws Exception {
        Set<String> list = new HashSet<>();
        list.add("test-ip");
        when(blackListService.getBlackList()).thenReturn(list);
        Location location = locationService.getLocation("test-ip");
        assertNull(location);
    }

    @Test
    public void testNullCountry() throws Exception {
        when(countryService.getCountryInfo(anyString())).thenReturn(null);
        Location location = locationService.getLocation("test-ip");
        assertNull(location);
    }

    @Test
    public void testNullCurrency() throws Exception {
        CountryResponse countryResponse = new CountryResponse();
        countryResponse.setCountry("test-country");
        countryResponse.setCountryCode("test-code");
        countryResponse.setCurrency("test-currency");
        when(countryService.getCountryInfo(anyString())).thenReturn(countryResponse);
        when(currencyService.getCurrencyInfo(anyString())).thenReturn(null);
        Location location = locationService.getLocation("test-ip");
        assertNull(location);
    }

    @Test
    public void testGetLocation() throws Exception {
        CountryResponse countryResponse = new CountryResponse();
        countryResponse.setCountry("test-country");
        countryResponse.setCountryCode("test-code");
        countryResponse.setCurrency("test-currency");
        CurrencyResponse currencyResponse = new CurrencyResponse();
        Map<String, CurrencyResponse.Currency> map = new HashMap<>();
        CurrencyResponse.Currency currency = new CurrencyResponse.Currency();
        currency.setFr("COP");
        currency.setId("test-currency");
        currency.setTo("USD");
        BigDecimal value = new BigDecimal(10l);
        currency.setVal(value);
        map.put("COP", currency );
        currencyResponse.setResults(map);
        when(countryService.getCountryInfo(anyString())).thenReturn(countryResponse);
        when(currencyService.getCurrencyInfo(anyString())).thenReturn(currencyResponse);

        Location location = locationService.getLocation("test-ip");
        assertNotNull(location);
        assertEquals("test-currency", location.getCurrency().getBase());
        assertEquals(value, location.getCurrency().getRates().get("USD"));
        assertEquals("test-country", location.getCountry().getName());
        assertEquals("test-code", location.getCountry().getCode());
    }
}