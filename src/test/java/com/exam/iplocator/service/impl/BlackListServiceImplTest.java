package com.exam.iplocator.service.impl;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

@RunWith(SpringRunner.class)
public class BlackListServiceImplTest extends TestCase {

    @TestConfiguration
    static class BlackListServiceImplTestContextConfiguration {

        @Bean
        public BlackListServiceImpl blackListService() {
            return new BlackListServiceImpl();
        }
    }

    @Autowired
    private BlackListServiceImpl blackListService;

    @Test
    public void testBlackList() {
        Set<String> list = blackListService.getBlackList();
        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertTrue(list.contains("190.250.110.215"));
    }

    @Test
    public void testNotFound() {
        Set<String> list = blackListService.getBlackList();
        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertFalse(list.contains("190.250.110.21"));
    }
}