package com.exam.iplocator.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
@AllArgsConstructor
public class Location implements Serializable {

    private Country country;
    private Currency currency;

    @Data
    @AllArgsConstructor
    public static class Country implements Serializable {
        private String name;
        private String code;
    }

    @Data
    @AllArgsConstructor
    public static class Currency implements Serializable {
        private String base;
        private Map<String, Object> rates;
    }
}
