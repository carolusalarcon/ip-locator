package com.exam.iplocator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class IPLocatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(IPLocatorApplication.class, args);
	}

}
