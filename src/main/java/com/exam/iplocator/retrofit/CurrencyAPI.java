package com.exam.iplocator.retrofit;

import com.exam.iplocator.retrofit.model.CurrencyResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface CurrencyAPI {

    @GET("convert")
    @Headers({
            "content-type: application/json",
            "accept: application/json"
    })
    Call<CurrencyResponse> getCurrencyInfo(@Query("q") String query, @Query("apiKey") String apiKey);
}
