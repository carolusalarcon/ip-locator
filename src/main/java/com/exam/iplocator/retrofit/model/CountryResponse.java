package com.exam.iplocator.retrofit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryResponse {

    private String status;
    private String country;
    private String countryCode;
    private String currency;
    private String query;
}
