package com.exam.iplocator.retrofit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrencyResponse {

    private Map<String, Currency> results;

    @Data
    public static class Currency {
        private String id;
        private String fr;
        private String to;
        private BigDecimal val;
    }
}
