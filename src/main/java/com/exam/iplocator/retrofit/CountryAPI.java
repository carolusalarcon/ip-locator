package com.exam.iplocator.retrofit;

import com.exam.iplocator.retrofit.model.CountryResponse;
import retrofit2.Call;
import retrofit2.http.*;

public interface CountryAPI {

    @GET("/json/{ip}")
    @Headers({
            "content-type: application/json",
            "accept: application/json"
    })
    Call<CountryResponse> getCountryInfo(@Path("ip") String ip, @Query("fields") String fields);
}
