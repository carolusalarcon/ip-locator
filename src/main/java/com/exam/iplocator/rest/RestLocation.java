package com.exam.iplocator.rest;

import com.exam.iplocator.model.Location;
import com.exam.iplocator.service.LocationService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequestMapping("api/v1/")
public class RestLocation {

    @Autowired
    private LocationService locationService;

    @GetMapping(value = "location/{ip}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Location> getLocation(@PathVariable String ip) {
        Location location;
        if (isValidIP(ip)) {
            location = locationService.getLocation(ip);
            if (location == null) {
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity(location, HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Validates IPv4 or IPv6 address.
     * @param ip Param IP value
     * @return false if the given IP is not a valid IPv4 or IPv6 address.
     */
    private boolean isValidIP(String ip) {
        InetAddressValidator validator = InetAddressValidator.getInstance();
        if (validator.isValid(ip)) {
            return true;
        } else {
            log.error("The IP address {} is not valid", ip);
            return false;
        }
    }
}
