package com.exam.iplocator.config;

import com.exam.iplocator.retrofit.CountryAPI;
import com.exam.iplocator.retrofit.CurrencyAPI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Configuration
public class RetrofitConfig {

    @Bean
    public CountryAPI getCountryInfo() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ip-api.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(CountryAPI.class);
    }

    @Bean
    public CurrencyAPI getCurrencyInfo() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://free.currconv.com/api/v7/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(CurrencyAPI.class);
    }
}
