package com.exam.iplocator.service;

import com.exam.iplocator.retrofit.model.CurrencyResponse;

public interface CurrencyService {

    /**
     * Returns currency Information
     * @param currency
     * @return CurrencyResponse
     * @throws Exception
     */
    CurrencyResponse getCurrencyInfo(String currency) throws Exception;
}
