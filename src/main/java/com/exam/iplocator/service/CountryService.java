package com.exam.iplocator.service;

import com.exam.iplocator.retrofit.model.CountryResponse;

public interface CountryService {

    /**
     * Returns Country info by IP
     * @param ip
     * @return CountryResponse
     * @throws Exception
     */
    CountryResponse getCountryInfo(String ip) throws Exception;
}
