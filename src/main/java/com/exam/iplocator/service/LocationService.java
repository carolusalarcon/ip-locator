package com.exam.iplocator.service;

import com.exam.iplocator.model.Location;

public interface LocationService {

    /**
     * Returns Location Country and Currency by IP
     * @param ip
     * @return Location
     */
    Location getLocation(String ip);
}
