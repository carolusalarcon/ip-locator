package com.exam.iplocator.service;

import org.springframework.cache.annotation.Cacheable;

import java.util.Set;

public interface BlackListService {

    /**
     * Returns a list of Blacklist IP
     * @return Set
     */
    @Cacheable(value = "blacklistCache")
    Set<String> getBlackList();
}
