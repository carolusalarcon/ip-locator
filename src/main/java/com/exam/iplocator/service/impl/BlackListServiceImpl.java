package com.exam.iplocator.service.impl;

import com.exam.iplocator.service.BlackListService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
@Service
public class BlackListServiceImpl implements BlackListService {

    @Value("${blacklist.file.path:src/main/resources/blacklist}")
    private String filePath;

    /**
     * Read File and map blacklist to Set
     * @return
     */
    @Cacheable(value = "blacklistCache")
    @Override
    public Set<String> getBlackList() {
        try {
            Path path = FileSystems.getDefault().getPath(filePath);
            log.info("Retrieve blacklist from file");
            return Files.lines(path).collect(Collectors.toSet());
        } catch (Exception e) {
            log.error("Error reading blacklist file");
        }

        return Collections.emptySet();
    }
}
