package com.exam.iplocator.service.impl;

import com.exam.iplocator.retrofit.CountryAPI;
import com.exam.iplocator.retrofit.model.CountryResponse;
import com.exam.iplocator.service.CountryService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Call;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Log4j2
@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryAPI countryAPI;

    @Override
    public CountryResponse getCountryInfo(String ip) throws Exception {

        //String fields to call country service.
        String fields = "status,message,country,countryCode,currency,query";
        Call<CountryResponse> call = countryAPI.getCountryInfo(ip, fields);
        try {
            return Optional.ofNullable(call.execute()).map(response -> {

                if (!response.isSuccessful()) {
                    String errorBody = null;
                    if (Objects.nonNull(response.errorBody())) {
                        try {
                            errorBody = response.errorBody().string();
                        } catch (Exception e) {
                            log.error(e);
                        }
                    }
                    log.error(errorBody);
                }

                return response.body();
            }).orElseThrow(() ->
                    new Exception("Error in call to Country Service")
            );
        } catch (IOException e) {
            throw e;
        }
    }
}
