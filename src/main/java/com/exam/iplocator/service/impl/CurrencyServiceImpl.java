package com.exam.iplocator.service.impl;

import com.exam.iplocator.retrofit.CurrencyAPI;
import com.exam.iplocator.retrofit.model.CurrencyResponse;
import com.exam.iplocator.service.CurrencyService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Log4j2
@Service
public class CurrencyServiceImpl implements CurrencyService {

    private static final String USD = "USD";
    private static final String EUR = "EUR";

    @Autowired
    private CurrencyAPI currencyAPI;

    /**
     * This key should be stored in other service like Vault or DB
     */
    @Value("${currency.api.key:a6f5ae4b90fd5a17e1f3}")
    private String apiKey;

    @Override
    public CurrencyResponse getCurrencyInfo(String currency) throws Exception {

        String query = String.format("%s_%s,%s_%s", currency, EUR, currency, USD);

        Call<CurrencyResponse> call = currencyAPI.getCurrencyInfo(query, apiKey);
        try {
            return Optional.ofNullable(call.execute()).map(response -> {

                if (!response.isSuccessful()) {
                    String errorBody = null;
                    if (Objects.nonNull(response.errorBody())) {
                        try {
                            errorBody = response.errorBody().string();
                        } catch (Exception e) {
                            log.error(e);
                        }
                    }
                    log.error(errorBody);
                }

                return response.body();
            }).orElseThrow(() ->
                    new Exception("Error in call to Country Service")
            );
        } catch (IOException e) {
            throw e;
        }
    }

}
