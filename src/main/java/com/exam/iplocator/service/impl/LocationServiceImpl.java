package com.exam.iplocator.service.impl;

import com.exam.iplocator.model.Location;
import com.exam.iplocator.retrofit.model.CountryResponse;
import com.exam.iplocator.retrofit.model.CurrencyResponse;
import com.exam.iplocator.service.BlackListService;
import com.exam.iplocator.service.CountryService;
import com.exam.iplocator.service.CurrencyService;
import com.exam.iplocator.service.LocationService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.ws.rs.InternalServerErrorException;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private CountryService countryService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private BlackListService blackListService;

    @Override
    @Cacheable(value = "ipCache", key = "#ip")
    public Location getLocation(String ip) {

        try {
            if (existsInBlacklist(ip)){
                log.warn("IP found in blacklist {}", ip);
                return null;
            }
            //Get information about country by IP
            CountryResponse countryResponse = countryService.getCountryInfo(ip);

            if (countryResponse == null || countryResponse.getCurrency() == null) {
                log.warn("No country information found for ip {}", ip);
                return null;
            }

            //Get information about country currency
            CurrencyResponse currencyResponse = currencyService.getCurrencyInfo(countryResponse.getCurrency());

            if (currencyResponse == null || currencyResponse.getResults() == null || currencyResponse.getResults().isEmpty()) {
                log.warn("No currency information found for ip {}", ip);
                return null;
            }

            log.info("Location info retrieved from services for ip {}", ip);
            return mapToLocation(countryResponse, currencyResponse);
        } catch (Exception e) {
            log.error(e);
            throw new InternalServerErrorException(e);
        }
    }

    /**
     * Maps countryResponse and currencyResponse to Location
     * @param countryResponse
     * @param currencyResponse
     * @return Location
     */
    private Location mapToLocation(CountryResponse countryResponse, CurrencyResponse currencyResponse) {
        // Maps currency information
        Map<String, Object> map = currencyResponse.getResults().entrySet().stream().collect(Collectors.toMap(
                e -> e.getValue().getTo(),
                e -> e.getValue().getVal()));

        Location.Country country = new Location.Country(countryResponse.getCountry(), countryResponse.getCountryCode());
        Location.Currency currency = new Location.Currency(countryResponse.getCurrency(), map);

        return new Location(country, currency);
    }

    /**
     * Validate if the given IP is into the blacklist
     * @param ip
     * @return boolean true if given IP is into the blacklist
     */
    private boolean existsInBlacklist(String ip) {
        Set<String> blacklist = blackListService.getBlackList();
        return blacklist.contains(ip);
    }

}
