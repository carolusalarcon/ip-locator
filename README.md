# ip-locator

**Build Image**

`mvn clean package`

**Build Image**

`docker build -t iplocator .`

**Run Docker**

`docker run -p 8080:8080 iplocator`

**API**

`http://localhost:8080/api/v1/location/{ip}`

Example

`http://localhost:8080/api/v1/location/190.250.110.219`
